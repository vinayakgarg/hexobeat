var hexgame,
	moveNum,
	nick,
	gameStarted,
	playerState,
	oppState,
	oppType,
	opponent,
	sigp,
	getPlaIntSet=0,
	gotResponse=1,
	ts;
var NUM_MC_SIM=1600;
var ROW_DIR=[-1, -1, 0, 1, 1, 0];
var COL_DIR=[0, 1, 1, 0, -1, -1];
var State={"EMPTY":1,"BLUE":2,"RED":3};

function getUrlParam(key){
	var result=new RegExp(key+"=([^&]*)","i").exec(window.location.search)
	return result&&unescape(result[1])||""
}
function Board(size){
	this.size=size;
	this.mat=new Array(size+2);
	for(var i=0;i<size+2;i++){
		this.mat[i]=new Array(size+2);
		for(var j=0;j<size+2;j++){
			this.mat[i][j]=State.EMPTY;
		}
	}
	this.occupied=function(row,col){
		console.log(this.mat[row][col]);
		console.log(State.EMPTY);
		return this.mat[row][col]!=State.EMPTY;
	}
	this.set=function(row,col,s){
		this.mat[row][col]=s;
		this.fillhexagon(row,col,s);
	}
	this.get=function(row,col){
		return this.mat[row][col];
	}
	this.fillhexagon=function(r,c,s){
		var cnv=$('#hexboard')[0],
			ctx=cnv.getContext('2d'),
			dx=cnv.width/2-(this.size/2)*Math.sqrt(3)*20*3/2,
			xc=dx+(r-1)*Math.sqrt(3)*20/2+c*Math.sqrt(3)*20,
			yc=25+3*r*20/2;
		ctx.beginPath();
		ctx.moveTo(xc+20*Math.cos(Math.PI/2),yc+20*Math.sin(Math.PI/2));          
		for(k=1;k<=6;k++){
			ctx.lineTo(xc+20*Math.cos(Math.PI/2+k*2*Math.PI/6),yc+20*Math.sin(Math.PI/2+k*2*Math.PI/6));
			ctx.strokeStyle='#000';
			ctx.lineWidth=1;
			ctx.stroke();
		}
		if(s==State.RED){
			ctx.fillStyle="#f00";
		}else{
			ctx.fillStyle="#00f";
		}
		ctx.fill();
		ctx.closePath();
	}
}
function DisjointSet(size){
	this.p=new Array(size);
	for(i=0;i<size;i++){
		this.p[i]=i;
	}
	this.root=function(a){
		while(a!=this.p[a]){
			this.p[a]=this.p[this.p[a]];
			a=this.p[a];
		}
		return a;
	}
	this.join=function(a,b){
		var ra=this.root(a);
		var rb=this.root(b);
		this.p[ra]=rb;
	}
	this.is_joint=function(a,b){
		return this.root(a)==this.root(b);
	}
}
function HEX(){
	this.size=$('#boardsize').getval()
	this.board=new Board(this.size)
	draw()
	this.blue_set=new DisjointSet(this.size*this.size+2)
	this.red_set=new DisjointSet(this.size*this.size+2)
	this.crow=1
	this.ccol=1
	if(playerState==State.RED)oppState=State.BLUE
	else oppState=State.RED
	this.move=function(r,c,s){
		this.board.set(r,c,s);
		moveNum++;
		for(var i=0;i<6;i++){
			var row=r+ROW_DIR[i],col=c+COL_DIR[i];
			if(this.board.get(row,col)==s){
				if (s==State.BLUE){
					this.blue_set.join((r-1)*this.size+c,(row-1)*this.size+col);
				}else{
					this.red_set.join((r-1)*this.size+c,(row-1)*this.size+col);
				}
			}
		}
		if(s==State.BLUE){
			if(c==1){
				this.blue_set.join(0,(r-1)*this.size+c);
			}else if(c==this.size){
				this.blue_set.join(this.size*this.size+1,(r-1)*this.size+c);
			}
		}else{
			if(r==1){
				this.red_set.join(0,(r-1)*this.size+c);
			}else if(r==this.size){
				this.red_set.join(this.size*this.size+1,(r-1)*this.size+c);
			}
		}
	}
	this.ai_move=function(){
		var r,c;
		moveNum++;
		do{r=Math.floor(Math.random()*this.size+1);c=Math.floor(Math.random()*this.size+1);}
		while(this.board.occupied(r,c));
		console.log('random='+r+','+c);
		this.board.set(r,c,oppState);
	}
	this.checkwinner=function(){
		if(this.red_set.is_joint(0,this.size*this.size+1))return State.RED;
		if(this.blue_set.is_joint(0,this.size*this.size+1))return State.BLUE;
		return State.EMPTY;
	}
}
function triangle(ctx,x1,y1,x2,y2,x3,y3,c){
	ctx.beginPath()
	ctx.moveTo(x1,y1)
	ctx.lineTo(x2,y2)
	ctx.lineTo(x3,y3)
	ctx.fillStyle=c
	ctx.fill()
}
function draw(){
	var cnv=$('#hexboard')[0],
		ctx=cnv.getContext('2d'),
		s=20,xc,yc=25,r=255,b=255,
		size=$('#boardsize').getval(),
		dx=cnv.width/2-(size/2)*Math.sqrt(3)*s*3/2;
	cnv.height=280+(size-7)*3*s/2;
	ctx.clearRect(0,0,cnv.width,cnv.height);
	triangle(ctx,cnv.width/2,cnv.height/2,dx-10,yc,dx+(size)*Math.sqrt(3)*s/2-10,yc+(size+1)*3*s/2,'blue')
	triangle(ctx,cnv.width/2,cnv.height/2,dx-10,yc,dx+(size)*Math.sqrt(3)*s+10,yc,'red')
	triangle(ctx,cnv.width/2,cnv.height/2,dx+(size)*Math.sqrt(3)*s+10,yc,dx+(size+1)*Math.sqrt(3)*3*s/2-10,yc+(size+1)*3*s/2,'blue')
	triangle(ctx,cnv.width/2,cnv.height/2,dx+(size)*Math.sqrt(3)*s/2-10,yc+(size+1)*3*s/2,dx+(size+1)*Math.sqrt(3)*3*s/2-10,yc+(size+1)*3*s/2,'red')
	for(i=0;i<size;i++){
		xc=dx+i*Math.sqrt(3)*s/2;
		yc+=3*s/2;
		for(j=0;j<size;j++){
			xc+=Math.sqrt(3)*s;
			ctx.beginPath();
			ctx.moveTo(xc+s*Math.cos(Math.PI/2),yc+s*Math.sin(Math.PI/2));          
			for(k=1;k<=6;k++){
				ctx.lineTo(xc+s*Math.cos(Math.PI/2+k*2*Math.PI/6),yc+s*Math.sin(Math.PI/2+k*2*Math.PI/6));
				ctx.strokeStyle='#000';
				ctx.lineWidth=1;
				ctx.stroke();
			}
			ctx.fillStyle="#"+(r-i).toString(16)+(255).toString(16)+(b-j).toString(16);
			ctx.fill();
			ctx.closePath();
		}
	}
}
function move(e){
	if(gameStarted){
		e=e||window.e;
		var cnv=$("#hexboard")[0],
			ctx=cnv.getContext('2d'),
			clr=ctx.getImageData(e.pageX-cnv.offsetLeft,e.pageY-cnv.offsetTop,1,1).data,
			row=256-clr[0],
			col=256-clr[2];
		if(row<=hexgame.size&&col<=hexgame.size){
			if((moveNum%2&&playerState==State.BLUE)||(moveNum%2==0&&playerState==State.RED)){
				hexgame.move(row,col,playerState)
				if(oppType!='ai'){
					sendMove(row,col)
				}
				if(hexgame.checkwinner()==State.EMPTY){
					if(oppType=='ai'){
						hexgame.ai_move()
					}else{
						setTimeout(getMove,15000)
					}
				}
				else{
					endgame()
					nexplayer('red',' ')
				}
			}
		}
	}
}
function msg(m){
	$('#msg .modal-body').html(m)
	$('#msg').modal()
}
function nexplayer(c,m){
	var cnv=$('#hexboard')[0],
		ctx=cnv.getContext('2d')
	ctx.clearRect(cnv.width-300,0,300,25);
	ctx.fillStyle=c
	ctx.font='bold 16px Arial'
	ctx.textAlign='right'
	ctx.fillText(m,cnv.width-10,20)
}
function startgame(){
	preparegame()
	oppType=$('input:radio[name=opponent]:checked').val()
	var color=$('input:radio[name=playingcolor]:checked').val()
	if(color=='red')playerState=State.RED
	else playerState=State.BLUE
	if(oppType=='ai'){
		hexgame=new HEX()
		if(playerState==State.RED){
			hexgame.ai_move()
		}
		if(logged=='1'){
			$.ajax({
				type:'GET',
				url:'/ajax/setstate',
				dataType:'json',
				data:'nick='+nick+'&state=playing'
			})
		}
	}else{
		opponent=$('.activeplayers').val()
		console.log('opponent='+opponent)
		if(opponent){
			$.ajax({
				type: 'GET',
				url: '/ajax/request',
				dataType: 'json',
				data: 'requester='+$('#nickname').html()+'&opponent='+opponent+'&color='+color+'&boardsize='+$('#boardsize').getval(),
				success: function(r){
					console.log(r)
					setTimeout(function(){getState(1,r.ts,opponent)},20000)
					ts=r.ts
				}
			})
		}else{
			cleanup()
		}
	}
}
function getState(c,ts,o){
	$.ajax({
		type:'GET',
		url:'/ajax/getstate',
		dataType:'json',
		data:'ts='+ts+'&opponent='+o,
		success: function(r){
			if(r&&r.state){
				console.log(r.state)
				switch(r.state){
					case 'initial':
						if(c){
							setTimeout(function(){getState(0,ts,o)},30000)
						}
						break
					case 'accepted':
						msg('Great news man, '+o+' accepted your request')
						hexgame=new HEX()
						if(playerState==State.RED){
							nexplayer('blue', 'Waiting for opponent\'s move')
						}else{
							nexplayer('blue', 'Waiting for your move')
						}
						break
					case 'rejected':
						msg('Sorry dude, but '+o+'(sunavabich) rejected your request')
					case 'expired':
						endgame()
				}
			}
			
		}
	})
}
function preparegame(){
	$('.startgame').addClass('disabled')
	$('#controls').hide(1000)
	gameStarted=true
	moveNum=1
	if(getPlaIntSet){
		clearInterval(sigp)
		getPlaIntSet=0
	}
}
function cleanup(){
	$('.startgame').removeClass('disabled')
	$('#controls').show(1000)
	gameStarted=false
	if(!getPlaIntSet&&oppType!='ai'){
		sigp=setInterval(getPlayers,30000)
		getPlaIntSet=1
	}
}
function endgame(){
	if(hexgame.checkwinner()==playerState)msg('You Won!!!')
	else msg('You lost. looser :p')
	if(logged=='1'){
		$.ajax({
			type:'GET',
			url:'/ajax/setstate',
			dataType:'json',
			data:'nick='+nick+'&state=idle'
		})
	}
	cleanup()
}
function savenick(e){
	nick=$('#selnickname').val()
	$.ajax({
		type: 'GET',
		url: '/ajax/setnick',
		dataType: 'json',
		data: 'nick='+nick,
		success: function(r){
			if(r.success){
				$('#nickname').html(nick)
				$('#nickmodal').modal('hide')
			}else{
				$('.nickalert').removeClass('hidden')
			}
		},
		error: function(r){
			console.log('failed')
		}
	})
}
function getPlayers(){
	$.ajax({
		type: 'GET',
		url: '/ajax/getplayers',
		dataType: 'json',
		success: function(r){
			if(r){
				$('.activeplayers').html('')
				for(var i=0;i<r.length;i++){
					$('.activeplayers').append('<option value="'+r[i].nick+'"'+(r[i].state=='idle'&&r[i].nick!=nick?'':'disabled')+'>'+r[i].nick+' ['+r[i].state+']</option>')
				}
			}
		}
	})
}
function acceptgame(r){
	gotResponse=1
	$.ajax({
		type: 'GET',
		url: '/ajax/accept',
		dataType: 'json',
		data: 'ts='+r.ts+'&opponent='+nick,
	})
	ts=r.ts
	$('input:radio[name=opponent][value=human]').click()
	$('#boardsize').slider('setValue',r.boardsize)
	if(r.reqcolor=='red'){
		$('input:radio[name=playingcolor][value=blue]').click()
		playerState=State.BLUE
	}else{
		$('input:radio[name=playingcolor][value=red]').click()
		playerState=State.RED
		setTimeout(getMove,30000)
	}
	preparegame()
	$('#gamerequest').modal('hide')
	hexgame=new HEX()
	if(playerState==State.RED){
		nexplayer('blue','Waiting for opponent\'s move')
	}else{
		nexplayer('blue','Waiting for your move')
	}
}
function rejectgame(r){
	if(!gameStarted){
		gotResponse=1
		$.ajax({
			type: 'GET',
			url: '/ajax/reject',
			dataType: 'json',
			data: 'ts='+r.ts+'&opponent='+nick,
		})
	}
}
function ping(){
	console.log(nick)
	$.ajax({
		type: 'GET',
		url: '/ajax/ping',
		dataType: 'json',
		data: 'nick='+nick,
		success: function(r){
			if(gotResponse&&r&&r.requester&&r.reqcolor&&r.boardsize&&r.ts){
				console.log(r.requester+'wants to play')
				var mycolor=(r.reqcolor=='red')?'blue':'red'
				$('#gamerequest .modal-title').html(r.requester+' wants to play Hex with you')
				$('#gamerequest .modal-body').html('You will play as '+mycolor+'<br>Board size will be '+r.boardsize)
				$('#gamerequest').modal({keyboard: false})
				$('#gamerequest').on('hidden.bs.modal',function(){rejectgame(r)})
				$('.acceptgame').on('click',function(e){acceptgame(r)})
				gotResponse=0
			}
		}
	})
}
function sendMove(r,c){
	$.ajax({
		type:'GET',
		url:'/ajax/setmove',
		dataType:'json',
		data:'ts='+ts+'&row='+r+'&col='+c
	})
	nexplayer(playerState==State.RED?'blue':'red','Waiting for opponent\'s move')
}
function getMove(){
	$.ajax({
		type:'GET',
		url:'/ajax/getmove',
		dataType:'json',
		data:'ts='+ts,
		success:function(r){
			if(r&&r.moveNum==moveNum){
				hexgame.move(r.row,r.col,oppState)
				if(hexgame.checkwinner()!=State.EMPTY){
					endgame()
					nexplayer('red',' ')
				}else{
					nexplayer(playerState==State.RED?'red':'blue','Waiting for your move')
				}
			}else{
				setTimeout(getMove,8000)
			}
		}
	})
}
$(document).ready(function(){
	$('#boardsize').slider()
	$('input:radio[name=playingcolor][value=blue]').click()
	$('input:radio[name=opponent][value=ai]').click()
	gameStarted=false
	draw()
	$('#boardsize').slider().on('slide', function(ev){
		draw()
	})
	$('input:radio[name=opponent]').change(function(){
		if($(this).val()=='human'&&logged=='0'){
			$('.login').removeClass('hidden')
			$('.startgame').addClass('disabled')
		}else{
			$('.login').addClass('hidden')
			$('.startgame').removeClass('disabled')
			if($(this).val()=='human'){
				getPlayers()
				$('.activeplayers').removeClass('hidden')
				if(!getPlaIntSet){
					sigp=setInterval(getPlayers, 30000)
					getPlaIntSet=1
				}
			}else{
				$('.activeplayers').addClass('hidden')
				if(getPlaIntSet){
					clearInterval(sigp)
					getPlaIntSet=0
				}
			}
		}
	})
	$('.login').click(function(){
		$(this).addClass('hidden')
		$('input:radio[name=opponent][value=ai]').click()
	})
	if(getnick){
		$('#nickmodal').modal({keyboard: false})
		$('.savenick').on('click',savenick)
	}else{
		nick=$('#nickname').html()
	}
	if(logged=='1'){
		setInterval(ping, 30000)
	}
});
