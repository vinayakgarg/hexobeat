import os
import urllib
import hashlib
import jinja2
import webapp2
import models

JINJA_ENVIRONMENT = jinja2.Environment(
    loader = jinja2.FileSystemLoader(os.path.dirname(__file__)+'/templates'),
    extensions = ['jinja2.ext.autoescape'],
    autoescape = True
)

class MainPage(webapp2.RequestHandler):

    def get(self):
        user_info = models.getUser(self)
        user_info['gravatar'] = hashlib.md5(user_info['email']).hexdigest()

        template = JINJA_ENVIRONMENT.get_template('header.html')
        self.response.write(template.render(user_info))
        template = JINJA_ENVIRONMENT.get_template('game.html')
        self.response.write(template.render(user_info))
        template = JINJA_ENVIRONMENT.get_template('footer.html')
        self.response.write(template.render())

class AboutPage(webapp2.RequestHandler):

    def get(self):
        user_info = models.getUser(self)
        user_info['gravatar'] = hashlib.md5(user_info['email']).hexdigest()
        
        template = JINJA_ENVIRONMENT.get_template('header.html')
        self.response.write(template.render(user_info))
        template = JINJA_ENVIRONMENT.get_template('about.html')
        self.response.write(template.render(user_info))
        template = JINJA_ENVIRONMENT.get_template('footer.html')
        self.response.write(template.render())        

class GetInvolvedPage(webapp2.RequestHandler):

    def get(self):
        user_info = models.getUser(self)
        user_info['gravatar'] = hashlib.md5(user_info['email']).hexdigest()
        
        template = JINJA_ENVIRONMENT.get_template('header.html')
        self.response.write(template.render(user_info))
        template = JINJA_ENVIRONMENT.get_template('getinvolved.html')
        self.response.write(template.render(user_info))
        template = JINJA_ENVIRONMENT.get_template('footer.html')
        self.response.write(template.render())        

application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/about', AboutPage),
    ('/getinvolved', GetInvolvedPage),
], debug=True)
