import models
import session
import webapp2
import logging
import json
import time
from datetime import datetime, timedelta
from google.appengine.api import users
from google.appengine.ext import ndb

restricted_nicks = ['admin', 'admininstrator', 'guest', 'root', 'nickname']

class SetNick(webapp2.RequestHandler):

	def get(self):
		nick = self.request.get('nick')
		if nick:
			cur_user = users.get_current_user()
			response = {'success' : 0}
			"""check if nick is restricted"""
			if nick not in restricted_nicks:
				"""check if nick is already in use"""
				user_query = models.User.query(ancestor=models.userNickKey(nick))
				user = user_query.get()
				if not user:
					user_query = models.User.query(ancestor=models.userKey(cur_user.email()))
					user = user_query.get()
					user.nick = nick
					user.nickSelected = True
					user.put()
					session.insertPlayer(nick)
					response['success'] = 1
			self.response.out.write(json.dumps(response))

class GetPlayers(webapp2.RequestHandler):

	def get(self):
		players_query = models.Players.query()
		players = []
		for player in players_query.fetch(20):
			if datetime.now() - player.ping < timedelta(minutes = 4):
				players.append({
					'nick':player.nick,
					'state':player.state
				})
			else:
				player.key.delete()
		self.response.out.write(json.dumps(players))

class Ping(webapp2.RequestHandler):

	def get(self):
		nick = self.request.get('nick')
		if nick:
			players_query = models.Players.query(ancestor=models.playersKey(nick))
			player = players_query.get()
			if player:
				player.put()
			else:
				session.insertPlayer(nick)
			""" Send response if someone wants to play with 'nick' """
			game_request_query = models.GameRequest.query(ancestor=models.gameRequestKey(nick))
			game_request = game_request_query.get()
			for game_request in game_request_query.fetch(10):
				logging.info('HI '+game_request.requester)
				if datetime.now() - game_request.reqTime < timedelta(minutes = 3) and game_request.state == 'initial':
					request = {
						'ts': game_request.ts,
						'requester': game_request.requester,
						'reqcolor' : game_request.reqColor,
						'boardsize': game_request.boardSize
					}
					self.response.out.write(json.dumps(request))
					break
				elif datetime.now() - game_request.reqTime > timedelta(minutes = 5):
					game_request.key.delete()

def setState(nick, state):
	players_query = models.Players.query(ancestor=models.playersKey(nick))
	player = players_query.get()
	if player:
		player.state = state
		player.put()

class SetState(webapp2.RequestHandler):

	def get(self):
		nick = self.request.get('nick')
		state = self.request.get('state')
		if nick and state:
			setState(nick, state)
	

class Request(webapp2.RequestHandler):

	def get(self):
		requester = self.request.get('requester')
		opponent = self.request.get('opponent')
		reqColor = self.request.get('color')
		boardSize = self.request.get('boardsize')
		if requester and opponent and reqColor and boardSize:
			gameRequest = models.GameRequest(parent=models.gameRequestKey(opponent))
			gameRequest.ts = int(time.time()*1000)
			gameRequest.requester = requester
			gameRequest.opponent = opponent
			gameRequest.reqColor = reqColor
			gameRequest.boardSize = int(boardSize)
			gameRequest.state = 'initial'
			gameRequest.put()
			players_query = models.Players.query(ancestor=models.playersKey(requester))
			player = players_query.get()
			if player:
				player.state = 'waiting'
				player.put()
			response = {
				'ts' : gameRequest.ts
			}
			self.response.out.write(json.dumps(response))

class StartGame(webapp2.RequestHandler):

	def get(self):
		ts = int(self.request.get('ts'))
		opponent = self.request.get('opponent')
		if ts and opponent:
			game_request_query = models.GameRequest.query(ancestor=models.gameRequestKey(opponent)).filter(models.GameRequest.ts == ts)
			game_request = game_request_query.get()
			if game_request:
				game_request.state = 'accepted'
				game_request.put()
				""" Create new game """
				cur_game = models.CurrentGames(parent=models.currentGamesKey(ts))
				cur_game.ts = ts
				cur_game.moveNum = 0
				if game_request.reqColor == 'blue':
					cur_game.blueNick = game_request.requester
					cur_game.redNick = game_request.opponent
				else:
					cur_game.blueNick = game_request.opponent
					cur_game.redNick = game_request.requester
				cur_game.put()
				""" Change player states """
				setState(game_request.requester, 'playing')
				setState(game_request.opponent, 'playing')

class RejectGame(webapp2.RequestHandler):

	def get(self):
		ts = int(self.request.get('ts'))
		opponent = self.request.get('opponent')
		if ts and opponent:
			game_request_query = models.GameRequest.query(ancestor=models.gameRequestKey(opponent)).filter(models.GameRequest.ts == ts)
			game_request = game_request_query.get()
			if game_request:
				game_request.state = 'rejected'
				game_request.put()
				""" Change player state """
				setState(game_request.requester, 'idle')

class GetState(webapp2.RequestHandler):

	def get(self):
		ts = int(self.request.get('ts'))
		opponent = self.request.get('opponent')
		if ts and opponent:
			game_request_query = models.GameRequest.query(ancestor=models.gameRequestKey(opponent)).filter(models.GameRequest.ts == ts)
			game_request = game_request_query.get()
			if game_request:
				response = {'state':game_request.state}
			else:
				response = {'state':'expired'}
			self.response.out.write(json.dumps(response))

class SetMove(webapp2.RequestHandler):

	def get(self):
		ts = int(self.request.get('ts'))
		row = int(self.request.get('row'))
		col = int(self.request.get('col'))
		if ts and row and col:
			cur_game_query = models.CurrentGames.query(ancestor=models.currentGamesKey(ts))
			cur_game = cur_game_query.get()
			if cur_game:
				cur_game.moveNum = cur_game.moveNum+1
				cur_game.row = row
				cur_game.col = col
				cur_game.put()

class GetMove(webapp2.RequestHandler):

	def get(self):
		ts = int(self.request.get('ts'))
		if ts:
			cur_game_query = models.CurrentGames.query(ancestor=models.currentGamesKey(ts))
			cur_game = cur_game_query.get()
			if cur_game:
				response = {
					'moveNum' : cur_game.moveNum,
					'row' : cur_game.row,
					'col' : cur_game.col
				}
				self.response.out.write(json.dumps(response))


application = webapp2.WSGIApplication([
    ('/ajax/setnick', SetNick),
    ('/ajax/getplayers', GetPlayers),
    ('/ajax/ping', Ping),
    ('/ajax/setstate', SetState),
    ('/ajax/request', Request),
    ('/ajax/accept', StartGame),
    ('/ajax/reject', RejectGame),
    ('/ajax/getstate', GetState),
    ('/ajax/setmove', SetMove),
    ('/ajax/getmove', GetMove),
], debug=True)

def main():
	webapp2.util.run_wsgi_app(application)

if __name__ == '__main__':
	main()