import models
import webapp2
from datetime import datetime, timedelta

def insertPlayer(nickname):
	player = models.Players(parent=models.playersKey(nickname))
	player.nick = nickname
	player.state = 'idle'
	player.put()

class Login(webapp2.RequestHandler):
	def get(self):
		redirect_url = self.request.get('r').encode('ascii')
		if redirect_url:
			user = models.getUser(self)
			if user['nickname'] != 'Guest' and user['getnick'] == '0':
				player_query = models.Players.query(ancestor=models.playersKey(user['nickname']))
				if not player_query:
					insertPlayer(user['nickname'])
				else:
					player = player_query.get()
					if not player:
						insertPlayer(user['nickname'])
					elif datetime.now() - player.ping > timedelta(minutes = 4):
						player.put()
			self.redirect(redirect_url)

application = webapp2.WSGIApplication([
    ('/login/', Login),
    #('/logout', Logout),
], debug=True)

def main():
	webapp2.util.run_wsgi_app(application)

if __name__ == '__main__':
	main()