from google.appengine.api import users
from google.appengine.ext import ndb

""" User """
def userKey(email):
	return ndb.Key('email', email)
def userNickKey(nick):
	return ndb.Key('nick', nick)

class User(ndb.Model):
    email = ndb.StringProperty()
    nick = ndb.StringProperty()
    nickSelected = ndb.BooleanProperty()
    gamesPlayed = ndb.IntegerProperty()


""" Stats """
def statsKey():
	return ndb.Key('key', 1)

class Stats(ndb.Model):
	key = ndb.IntegerProperty()
	userCount = ndb.IntegerProperty()
	gameCount = ndb.IntegerProperty()


""" Players """
def playersKey(nick):
	return ndb.Key('nick', nick)

#stores currently logged in users
class Players(ndb.Model):
	nick = ndb.StringProperty()
	ping = ndb.DateTimeProperty(auto_now=True)
	state = ndb.StringProperty()


""" GameRequest """
def gameRequestKey(opponent):
	return ndb.Key('opponent', opponent)

class GameRequest(ndb.Model):
	ts = ndb.IntegerProperty()							# unique identifier, equal to unix timestamp
	reqTime = ndb.DateTimeProperty(auto_now_add=True)	# Request time, used to clean enteries which are old
	requester = ndb.StringProperty()					# Nick of the requester
	opponent = ndb.StringProperty()						# Nick of the opponent
	reqColor = ndb.StringProperty()						# Colour choosen by requester
	boardSize = ndb.IntegerProperty()					# Size of the board
	state = ndb.StringProperty()						# initial | accepted | rejected


""" Game """
def currentGamesKey(ts):
	return ndb.Key('ts', ts)

class CurrentGames(ndb.Model):
	ts = ndb.IntegerProperty()							# unique identifier, equal to unix timestamp
	moveNum = ndb.IntegerProperty()						# move number
	blueNick = ndb.StringProperty()
	redNick = ndb.StringProperty()
	startTime = ndb.DateTimeProperty(auto_now_add=True)
	row = ndb.IntegerProperty()
	col = ndb.IntegerProperty()


def getUser(self):
	cur_user = users.get_current_user()
	if not cur_user:
		d = {
            'nickname' : 'Guest',
            'getnick' : '0',
            'email' : '',
            'logged' : '0',
            'log_in_url' : users.create_login_url('login/?r='+self.request.uri)
		}
		return d
	d = {
        'email' : cur_user.email(),
        'logged' : '1',
        'getnick' : '0',
        'log_out_url' : users.create_logout_url(self.request.uri)
        #'log_out_url' : 'logout/?r='+users.create_logout_url(self.request.uri)
	}
	user_query = User.query(ancestor=userKey(cur_user.email()))
	user = user_query.get()
	if not user:
		stats_query = Stats.query(ancestor=statsKey())
		stats = stats_query.get()
		if not stats:
			stats = Stats(parent=statsKey())
			stats.gameCount = 1
			stats.key = 1
			count = 1
		else:
			count = stats.userCount+1
		stats.userCount = count
		stats.put()
		user = User(parent=userKey(cur_user.email()))
		user.email = cur_user.email()
		user.nick = 'User'+str(count)
		user.nickSelected = False
		user.gamesPlayed = 0
		user.put()
		d['getnick'] = '1'
	elif not user.nickSelected:
		d['getnick'] = '1'

	d['nickname'] = user.nick
	return d
